from django import forms

class PostForm(forms.Form):
    titulo = forms.CharField(label="Titulo",max_length=100, widget=forms.TextInput(), required=True)
    conteudo = forms.CharField(label="Conteudo", max_length=1000, widget=forms.Textarea(
                                attrs={"rows":10, 'class' : 'form-control', 'style' : 'width:100%;'})
                               )
    #data_postagem = forms.CharField(required=False, max_length=20)
    titulo.widget.attrs.update({'class' : 'form-control' , 'placeholder' : 'Digite o titulo'})
    conteudo.widget.attrs.update({'class': 'form-control'})
