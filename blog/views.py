from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseNotFound
from .models import Post
from datetime import datetime, date
from .forms import PostForm
from django.views.generic.list import ListView
from django.views import generic

def index(request):
    return render(request, 'post/index.html')

def returnDateAtual():
    now = datetime.now()
    date_format = now.strftime('%d/%m/%Y')

    return date_format


def returnDateTime():
    now = datetime.now()

    return now

def list_posts(request):
    posts = Post.objects.all()

    context = {
        'data' : posts
    }

    return render(request, 'post/list_posts.html', context)

def index_without_form_django(request):
    context = {
        'data_post' : returnDateAtual()
    }

    return render(request, 'post/index_blog_without_form.html', context)

def index_with_form_django(request):
    print(request.method)

    if(request.method == 'POST'):
        form = PostForm(request.POST)

        if(form.is_valid()):
            titulo = form.cleaned_data['titulo']
            conteudo = form.cleaned_data['conteudo']
            data_postagem = returnDateTime()

            model = Post(titulo=titulo, conteudo=conteudo, data_postagem=data_postagem)
            model.save()

            return redirect('list_posts')
    else:
        form = PostForm()

        context = {
            'form' : form,
            'data_post': returnDateAtual()
        }

        return render(request, 'post/index_blog_with_form.html', context)

def post_with_form_django(request):
    form = request()

    return redirect('index_detalhe')

def post_without_form_django(request):
    titulo = request.POST.get("titulo", "")
    conteudo = request.POST.get("conteudo", "")
    data_postagem = returnDateTime()

    model = Post(titulo=titulo, conteudo = conteudo, data_postagem= data_postagem)
    model.save()

    return redirect('list_posts')

def index_detalhe(request, id):
    try:
        model = Post.objects.get(id=id)
    except:
        return HttpResponseNotFound()

    context = {
        'post': model
    }

    return render(request, 'post/index_detalhe.html', context)

def index_editar(request, id):
    model = Post.objects.get(id=id)

    context = {
        'post' : model
    }

    return render(request, 'post/index_editar.html', context)

def index_deletar(request, id):
    context = {
        'id' : id
    }

    return render(request, 'post/index_deletar.html', context)

def post_editar(request, id):
    titulo = request.POST.get("titulo", "")
    conteudo = request.POST.get("conteudo", "")

    model = Post.objects.get(id=id)

    model.titulo = titulo
    model.conteudo = conteudo

    model.save()

    return redirect('list_posts')

def post_deletar(request, id):
    model = Post.objects.get(id=id)

    model.delete()

    return redirect('list_posts')

# Generic Views
class PostListView(ListView):
    model = Post
    context_object_name = 'post_list'
    template_name = 'post/post_list.html'
    queryset = Post.objects.all()

class PostCreateView(generic.CreateView):
    model = PostForm()
    context_object_name = 'form'

    def get(self, request, *args, **kwargs):
        #print(self.object)
        #return render(request, template_name="post/post_create.html")

        context = {
            'form' : PostForm(),
            'data_post': returnDateAtual()
        }

        return render(request, "post/post_create.html", context)

    def post(self, request, *args, **kwargs):
        form = PostForm(request.POST)

        if (form.is_valid()):
            titulo = form.cleaned_data['titulo']
            conteudo = form.cleaned_data['conteudo']
            data_postagem = returnDateTime()

            model = Post(titulo=titulo, conteudo=conteudo, data_postagem=data_postagem)
            model.save()

        return redirect('list_post_generic')



class PostUpdateView(generic.UpdateView):
    model = PostForm

    def get(self, request, *args, **kwargs):
        form = PostForm()

        model = Post.objects.get(id=kwargs['pk'])

        form.fields["titulo"].initial = model.titulo
        form.fields["conteudo"].initial = model.conteudo

        context = {
            'form': form,
            'data_post': returnDateAtual()
        }

        return render(request, "post/post_update.html", context)

    def post(self, request, *args, **kwargs):
        form = PostForm(request.POST)

        if (form.is_valid()):
            model = Post.objects.get(id=kwargs['pk'])

            titulo = form.cleaned_data['titulo']
            conteudo = form.cleaned_data['conteudo']

            model.titulo = titulo
            model.conteudo = conteudo

            model.save()

        return redirect('list_post_generic')

class PostDetails(generic.DetailView):
    model = Post
    template_name = 'post/post_details.html'

    def post_detail_view(request, pk):

        try:
            post = Post.objects.get(id=pk)
        except Book.DoesNotExist:
            raise Http404('Post não existe')

        return render(request, context={'post': post})

class PostDeleteView(generic.DeleteView):
    model = Post

    def get(self, request, *args, **kwargs):

        context = {
            'id': kwargs['pk']
        }

        return render(request, "post/post_delete.html", context)

    def post(self, request, *args, **kwargs):
        model = Post.objects.get(id=kwargs['pk'])
        model.delete()

        return redirect('list_post_generic')