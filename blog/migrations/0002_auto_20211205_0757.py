# Generated by Django 3.1.3 on 2021-12-05 11:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=100)),
                ('conteudo', models.CharField(max_length=1000)),
                ('data_postagem', models.DateTimeField()),
            ],
        ),
        migrations.DeleteModel(
            name='Blog',
        ),
    ]
