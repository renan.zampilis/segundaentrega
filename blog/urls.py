from django.urls import path

from . import views
from .views import *

urlpatterns = [
    # Views Funcionais
    path('', views.index, name='index'),
    path('list_posts', views.list_posts, name='list_posts'),
    path('index_without_form_django', views.index_without_form_django, name='index_without_form_django'),
    path('index_with_form_django', views.index_with_form_django, name='index_with_form_django'),
    path('post_without_form_django', views.post_without_form_django, name='post_without_form_django'),
    path('index_detalhe/<int:id>/', views.index_detalhe, name='index_detalhe'),
    path('index_editar/<int:id>/', views.index_editar, name='index_editar'),
    path('index_editar/<int:id>/post_editar', views.post_editar, name='post_editar'),
    path('index_deletar/<int:id>/', views.index_deletar, name='index_deletar'),
    path('index_deletar/<int:id>/post_deletar', views.post_deletar, name='post_deletar'),

    #Views Generics
    path('list_post_generic/', PostListView.as_view(), name='list_post_generic'),
    path('list_post_generic/create_post_generic/', PostCreateView.as_view(), name='create_post_generic'),
    path('list_post_generic/update_post_generic/<int:pk>/', PostUpdateView.as_view(), name='update_post_generic'),
    path('list_post_generic/details_post_generic/<int:pk>/', PostDetails.as_view(), name='details_post_generic'),
    path('list_post_generic/delete_post_generic/<int:pk>/', PostDeleteView.as_view(), name='delete_post_generic'),
]